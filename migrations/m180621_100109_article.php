<?php

use yii\db\Migration;

/**
 * Class m180621_100109_article
 */
class m180621_100109_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'descriptin' => $this->string(),
            'body' => $this->text(),
            'author_id' => $this->integer(),
            'editor_id' => $this->integer(),
            'category_id' => $this->integer(),
        
     'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_100109_article cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_100109_article cannot be reverted.\n";

        return false;
    }
    */
}

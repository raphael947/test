<?php

use yii\db\Migration;

/**
 * Class m180621_100437_users
 */
class m180621_100437_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
            'email' => $this->string(),
            'username' => $this->string()->unique(),
            'auth_key' => $this->string(),
            'password' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_100437_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_100437_users cannot be reverted.\n";

        return false;
    }
    */
}
